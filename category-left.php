<?php
get_header();
?>
<?php 
                            
    query_posts('category_name=left'); ?>
    <?php while((have_posts())) : the_post();?>

        <section class="posts" id="post-<?php the_ID(); ?>">
          <div class="container">
            <div class="row">
              <div class="col-md-12 post-wrapper">
                
                <?php
                     if ( has_post_thumbnail() ) : ?>
                        <a href="<?php the_permalink() ?>">
                        <?php the_post_thumbnail(); ?>
                        </a>
                   <?php  endif;
                ?>
                
                <div class="post-body">
                <p><?php the_content() ?></p>

                  <div class="exclusive_eilings_box_info">
                        <a href="<?php the_permalink() ?>">Подробнее</a>
                </div>
                </div>
                <div class="date">
                  <p><?php echo get_the_date( 'r' ); ?>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section> 


                                
    <?php endwhile; ?>

<?php
get_footer();