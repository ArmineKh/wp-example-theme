<?php 
/**
 * Template name: Template 2
 */
get_header();
?>
​
​
<section>
    <div class="container">
        <div class="row">
        <div >
            <?php 
           
            query_posts('category_name=left'); 
                while ( have_posts() ) : the_post();?>



        <section class="posts" id="post-<?php the_ID(); ?>">
        <div class="container">
          <div class="row">
            <div class="col-md-12 post-wrapper">
             
              <?php
                   if ( has_post_thumbnail() ) :
                     the_post_thumbnail();
                   endif;
              ?>
              <div class="post-body">
              
              <p>
              <?php the_content() ?>
                
               </p> 
              </div>
              <div class="date">
                <p><?php echo get_the_date( 'r' ); ?>
                </p>
              </div>
            </div>
          </div>
        </div>
      </section> 
​
                <?php endwhile; // End of the loop.?>
        </div>
        </div>
    </div>
</section>
​
​
​
​
<?php get_footer(); ?>
