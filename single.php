<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php

			// Start the Loop.
			while ( have_posts() ) :
                the_post();

                ?>
                
                <section class="posts" id="post-<?php the_ID(); ?>">
          <div class="container">
            <div class="row">
              <div class="col-md-12 post-wrapper">
                
                <?php
                     if ( has_post_thumbnail() ) :
                       the_post_thumbnail();
                     endif;
                ?>
                <div class="date">
                  <p><?php echo get_the_date( 'r' ); ?>
                  </p>
                </div>
                <div class="post-body">
                <p><?php the_content() ?></p>
                  
                </div>
              </div>
            </div>
          </div>
        </section> 
              
        
                    <?php
                
				

			endwhile; // End the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
