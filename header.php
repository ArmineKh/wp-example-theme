<?php
/**
 * Шаблон шапки (header.php)
 * @package WordPress
 * @subpackage example-theme
 */
?>

<html lang="en">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,500;0,900;1,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title><?php echo wp_get_document_title(); ?></title>
    <?php wp_head();?>
</head>
<body <?php body_class();?>>
    <section class="introduction">
    <div class="row">
				<div class="col-md-12">

<nav class="navbar navbar-expand-lg navigation">
        
  <a class="navbar-brand" href="#">
  <div class="logo">
    <div class="logo-img">
        <img src="<?php echo get_template_directory_uri(); ?>/img/A-Kran.png" alt="">
    </div>
    <div class="logo-phone">
        <p>(068) 870-25-45</p>
    </div>
    </div>
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="fa fa-bars"></span>
  </button>
        <?php wp_nav_menu(array(
            'menu'       => 'top-menu',
            'menu_class' => 'navbar-nav mr-auto', 
            'depth' => 2,
            'container_id' => 'navbarSupportedContent',
				    'container_class' => 'collapse navbar-collapse',
            'items_wrap' => '<ul id="%1$s" class="%2$s" >%3$s</ul>',
            
            'walker'	 => new Bootstrap_Walker_Nav_Menu()
        ));?> 
</nav>
</div>
</div>
<div class="container">
<div class="row">
  <div class="introduction-title">
      <h1>Аренда крана-манипулятора в Киеве и области</h1>
  </div>
</div>
<div class="row text-center">
  <div class="phone-field">
    <form class="form-inline">
      <input class="form-control mr-sm-2" type="text"  placeholder="+ 380 000 00 00" >
      <button class="btn" type="submit">Заказать</button>
    </form>
  </div>
</div>



<div class="row">
  <div class="calculate">
     <a href="#">хотите Расчитать стоимость?</a>
  </div>
</div>
        <div class="elipse-wrapper">
            <div class="elipse">
            </div>
        </div>
        <div class="manipulator-wrapper">
            <img src="<?php echo get_template_directory_uri(); ?>/img/manipulators_f 1.png" alt="">
        </div>

        </div>
    </section>
    

