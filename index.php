<!DOCTYPE html>
<?php get_header(); ?>


        <section class="calculator">
          <div class="row">
            <div class="container">
              <div class="row">
              <div class="col-md-12 text-center">
                  <div class="calculator-header text-center">
                    <h2>Калькуляторы кнан манипулятор</h2>
                    <P>*Выберите калькулятор и введите данные в поле 
                        "Предположительное врнмя работы" и нажмите “Расчитать”</P>
                  </div>
                  <div class="calculator-tabs">
                    <div class="tab-component text-center align-middle">
                      <p>до 20 км от КП</p>
                    </div>
                    <div class="tab-component">
                      <p>от 20 км от КП</p>
                    </div>
                  </div>
              </div>
              </div> <!-- row -->

              <div class="befor-20" >

              <div class="row">
                <div class="col-md-12">
                <div class="flex-table-header">Место работы Киевская обл. (до 20 км от кп)</div>
                <div class="table-container" role="table" aria-label="Destinations">
                  <div class="flex-table row" role="rowgroup">
                  <div class="flex-row first" role="cell">Тоннаж габариты д\ш\в:</div>
                  <div class="flex-row" role="cell">До 6 тонн 6,5\2,46\2,65</div>
                  <div class="flex-row" role="cell">До 13 тонн 6,5\2,46\2,65</div>
                  <div class="flex-row" role="cell">Свыше 13 тонн 13,0\2,46\2,65</div>
                </div>
                <div class="flex-table row" role="rowgroup">
                  <div class="flex-row first" role="cell">Подача авто, грн:</div>
                  <div class="flex-row" role="cell">400</div>
                  <div class="flex-row" role="cell">450</div>
                  <div class="flex-row" role="cell">500</div>
                </div>
                <div class="flex-table row" role="rowgroup">
                  <div class="flex-row first"  role="cell"> Стоимость работы, грн\час:</div>
                  <div class="flex-row" role="cell">400</div>
                  <div class="flex-row" role="cell">450</div>
                  <div class="flex-row" role="cell">500</div>
                </div>
                <div class="flex-table row">
                  <div class="flex-row first" role="cell">Предположительное время работы, часы:</div>
                  <div class="flex-row" role="cell"></div>
                  <div class="flex-row" role="cell"></div>
                  <div class="flex-row" role="cell"></div>
                </div>
                
                </div>
                </div> <!-- col-md-12 -->
              </div> <!-- row  -->
            
              <div class="calculate-btn-wrammer">
                <a href="#" class="btn btn-block calculate-btn">Расчитать</a>
              </div>

              <div class="table-container" role="table" aria-label="Destinations">
                <div class="flex-table row">
                  <div class="flex-row first" role="cell">Сумма Вашего заказа:</div>
                  <div class="flex-row" role="cell">0 грн</div>
                  <div class="flex-row" role="cell">0 грн</div>
                  <div class="flex-row" role="cell">0 грн</div>
                </div>
                <div class="flex-table row">
                  <div class="flex-row first" role="cell">* Минимальный заказ:</div>
                  <div class="flex-row" role="cell">1400 грн</div>
                  <div class="flex-row" role="cell">1400 грн</div>
                  <div class="flex-row" role="cell">1400 грн</div>
                </div>
                </div>
                        
              <div class="phone-field">
                  <form class="form-inline calculate-phone-field">
                      <input class="form-control mr-sm-2" type="text"  placeholder="+ 380 000 00 00" >
                      <button class="btn my-2 my-sm-0" type="submit">Заказать</button>
                  </form>
              </div>


              </div> <!-- befor-20 -->


              <div class="after-20" style="display:none;">

              <div class="row">
                <div class="col-md-12">
                <div class="flex-table-header">Место работы Киевская обл. (до 20 км от кп)</div>
                <div class="table-container" role="table" aria-label="Destinations">
                  <div class="flex-table row" role="rowgroup">
                  <div class="flex-row first" role="cell">Тоннаж габариты д\ш\в:</div>
                  <div class="flex-row" role="cell">До 6 тонн 6,5\2,46\2,65</div>
                  <div class="flex-row" role="cell">До 13 тонн 6,5\2,46\2,65</div>
                  <div class="flex-row" role="cell">Свыше 13 тонн 13,0\2,46\2,65</div>
                </div>
                <div class="flex-table row" role="rowgroup">
                  <div class="flex-row first" role="cell">Подача авто, грн:</div>
                  <div class="flex-row" role="cell">0</div>
                  <div class="flex-row" role="cell">0</div>
                  <div class="flex-row" role="cell">0</div>
                </div>
                <div class="flex-table row" role="rowgroup">
                  <div class="flex-row first"  role="cell"> Стоимость работы, грн\час:</div>
                  <div class="flex-row" role="cell">250</div>
                  <div class="flex-row" role="cell">300</div>
                  <div class="flex-row" role="cell">350</div>
                </div>
                <div class="flex-table row">
                  <div class="flex-row first" role="cell">Время работы (мин. 2 часа):</div>
                  <div class="flex-row" role="cell">2</div>
                  <div class="flex-row" role="cell">2</div>
                  <div class="flex-row" role="cell">2</div>
                </div>
                <div class="flex-table row">
                  <div class="flex-row first" role="cell">Растояние, КМ (Киев - место загрузки - место выгрузки - Киев):</div>
                  <div class="flex-row" role="cell">--</div>
                  <div class="flex-row" role="cell">--</div>
                  <div class="flex-row" role="cell">--</div>
                </div>
                </div>
                </div> <!-- col-md-12 -->
              </div> <!-- row  -->
            
              <div class="calculate-btn-wrammer">
                <a href="#" class="btn btn-block calculate-btn">Расчитать</a>
              </div>

              <div class="table-container" role="table" aria-label="Destinations">
                <div class="flex-table row">
                  <div class="flex-row first" role="cell">Сумма Вашего заказа:</div>
                  <div class="flex-row" role="cell">0 грн</div>
                  <div class="flex-row" role="cell">0 грн</div>
                  <div class="flex-row" role="cell">0 грн</div>
                </div>
                <div class="flex-table row">
                  <div class="flex-row first" role="cell">* Минимальный заказ:</div>
                  <div class="flex-row" role="cell">500 грн +14 грн/км</div>
                  <div class="flex-row" role="cell">600 грн +16 грн/км</div>
                  <div class="flex-row" role="cell">700 грн + 18 грн/км</div>
                </div>
                </div>
                        
              <div class="phone-field">
                  <form class="form-inline calculate-phone-field">
                      <input class="form-control mr-sm-2" type="text"  placeholder="+ 380 000 00 00" >
                      <button class="btn my-2 my-sm-0" type="submit">Заказать</button>
                  </form>
              </div>


              </div> <!-- after-20 -->


          </div> <!-- end container -->
          </div> <!-- end row  -->
        </section>


<section class="rent-section" id="rent-section">
    <div class="row">
      <div class="container">
        <div class="col-12">
          <div class="rent-title">
            <h2>Аренда и продажа бытовок в Киеве и области</h2>
          </div>
          <div class="phone-field">
                  <form class="form-inline calculate-phone-field">
                      <input class="form-control mr-sm-2" type="text"  placeholder="+ 380 000 00 00" >
                      <button class="btn my-2 my-sm-0" type="submit">Заказать</button>
                  </form>
              </div>
        </div>
      </div>
    </div>
    <div class="row rent-img">
    <img src="<?php echo get_template_directory_uri(); ?>/img/img_3 1.png" alt="">
    </div>

</section>

<section class="rental-price-calculate">
  <div class="container">
    <div class="row">

  <div class="col-md-12">
    <div class="rental-price-title">
      <h2>Цена аренды бытовки, расчитать:</h2>
    </div>
  <div class="flex-table-header">Калькулятор аренды бытовки</div>
  <div class="table-container" role="table" aria-label="Destinations">
    <div class="flex-table row" role="rowgroup">
    <div class="flex-row first" role="cell">Комплектация:</div>
    <div class="flex-row" role="cell">Кол-во</div>
    <div class="flex-row" role="cell">Дней</div>
    <div class="flex-row" role="cell">Цена в день</div>
    <div class="flex-row" role="cell">Сумма грн.</div>
  </div>
  <div class="flex-table row" role="rowgroup">
    <div class="flex-row first" role="cell">Бытовка 6,0х2,5х2,5</div>
    <div class="flex-row" role="cell">---</div>
    <div class="flex-row border-bottom-none" role="cell"></div>
    <div class="flex-row" role="cell">70</div>
    <div class="flex-row" role="cell">0</div>

  </div>
  <div class="flex-table row" role="rowgroup">
    <div class="flex-row first"  role="cell">Кровати двухъярусные</div>
    <div class="flex-row" role="cell">---</div>
    <div class="flex-row" role="cell">---</div>
    <div class="flex-row" role="cell">120</div>
    <div class="flex-row" role="cell">0</div>

  </div>
  <div class="flex-table row">
    <div class="flex-row first" role="cell">Постель (подушка, матрац, одеяло)</div>
    <div class="flex-row" role="cell">---</div>
    <div class="flex-row border-bottom-none" role="cell"></div>
    <div class="flex-row" role="cell">90</div>
    <div class="flex-row" role="cell">0</div>
  </div>
  <div class="flex-table row">
    <div class="flex-row first" role="cell">Конвектор электрический</div>
    <div class="flex-row" role="cell">---</div>
    <div class="flex-row border-bottom-none" role="cell"></div>
    <div class="flex-row" role="cell">60</div>
    <div class="flex-row" role="cell">0</div>
  </div>
  </div>
  </div> <!-- col-md-12 -->


<div class="col-md-12">
  <div class="calculate-btn-wrammer">
    <a href="#" class="btn btn-block calculate-btn">Расчитать</a>
  </div>
</div>

<div class="table-container sum-order" role="table" aria-label="Destinations">
  <div class="flex-table row">
    <div class="flex-row first" role="cell">Сумма Вашего заказа:</div>
    <div class="flex-row" role="cell">0 грн</div>
  </div>
  </div>
          
<div class="phone-field">
    <form class="form-inline calculate-phone-field">
        <input class="form-control mr-sm-2" type="text"  placeholder="+ 380 000 00 00" >
        <button class="btn my-2 my-sm-0" type="submit">Заказать</button>
    </form>
</div>

    </div> <!-- row -->
  </div> <!-- container -->
</section>
<section class="benefits">
  <div class="container">
    <div class="row benefits-title">
      <h2>Преимущества? </h2>
    </div>
    <div class="row benefits-icons">
      <div class="icon-item">
        <img src="<?php echo get_template_directory_uri(); ?>/img/kran 1.png" alt="">
      </div>
      <div class="icon-item text-center"><p>Сам гружу</p></div>
      <div class="icon-item">
        <img src="<?php echo get_template_directory_uri(); ?>/img/drive 1.png" alt="">
      </div>
      <div class="icon-item text-center"><p>Сам везу</p></div>
      <div class="icon-item">
        <img src="<?php echo get_template_directory_uri(); ?>/img/speed 1.png" alt="">
      </div>
      <div class="icon-item text-center"><p>Работаем 24 часа в сутки</p></div>
    </div>
  </div>
</section>

<section class="body-text-1">
  <div class="container">
    <div class="row text-wrapper">
      <div class="title-wrapper">
        <h2>Кран манипулятор практичное средство для погрузки и разгрузки</h2>
      </div>
      <div class="paragraph-wrapper">
        <p>Если вы нуждаетесь в погрузке и перевозке заказывайте аренду крана манипулятора. Это устройство, которое комбинирует в себе функциональные возможности обычного автокрана и грузовика. Благодаря своим размерам, кран манипулятор отличается прекрасной маневренностью, что даёт возможность совершать перевозку габаритного груза по трассам с самым большим трафиком. Мы предлагаем вам наилучшие модели на самых выгодных условиях!</p>
        <p> Манипулятор (Киев) даёт возможность производить загрузочные и разгрузочные операции в ограниченном рабочем пространстве . Краны манипуляторы дают пользователю возможность проводить огромное количество маневров, благодаря чему можно найти решение даже в самой сложной ситуации, и успешно завершить погрузочную или разгрузочную работу. </p>
        <p> В нашей компании аренда крана манипулятора предлагается клиентам по самой низкой в Киеве цене. При этом, наш клиент получает полноценную поддержку при заказе данной услуги. Кроме того, все модели, которые мы готовы сдать в аренду находятся полностью в исправном состоянии. Заказывайте аренду крана манипулятора у нас сотрудничать с нами надёжно и выгодно!</p>
      </div>
    </div>
    <div class="row video-wrapper">
    <iframe width="1100px" height="500px" src="https://www.youtube.com/embed/1NcBmLdd3yI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    <!-- <video width="1100px" height="500px" controls>
      <source src="movie.mp4" type="video/mp4">
    </video> -->
    </div>
    <div class="row text-wrapper">
      <div class="title-wrapper">
        <h2>Преимущества работы нашего крана манипулятора</h2>
      </div>
      <div class="paragraph-wrapper">
        <p>Аренда манипулятора (Киев) сейчас набирает популярность, так как такая разновидность специализированной техники имеет ряд преимуществ:</p>
        <p>максимально автоматизирует процесс выгрузки и погрузки габаритного груза, что обеспечивает большую оперативность работ;</p>
        <p>даёт возможность без особых трудностей двигаться на самых загруженных дорогах города благодаря своей маневренности;</p>
        <p>минимизирует затраты времени, которые уходят на процесс загрузки и разгрузки; благодаря хорошей проходимости техники, можно добраться 
          до строительных объектов, которые являются труднодоступными.</p>
      </div>
    </div>
  </div>
</section>

<section class="results">
      <div class="container">
        <div class="row results-container">
        <div class="results-item">
          <div class="number">
            3700
          </div>
          <p>Перевозок за 7 лет</p>
        </div>
        <div class="results-item ">
          <div class="number">
            550
          </div>
          <p>Постоянных клиентов</p>
        </div>
        <div class="results-item">
          <div class="number">
            25
          </div>
          <p>Населенных пунктов Киевской области</p>
        </div>
      </div>
      </div>
</section>
<section class="body-text-2">
  <div class="container">
  <div class="row text-wrapper">
      <div class="title-wrapper">
        <h2>Зачем нужен кран манипулятор в Киеве? </h2>
      </div>
      <div class="paragraph-wrapper">
        <p>Кран манипулятор это специальная конструкция, которая совмещает в себе практичные 
          устройства из автокрана и грузовика. Таким образом, манипулятор оборудован кузовом 
          грузовика и непосредственно краном. Последний используется для загрузки и выгрузки 
          габаритного груза. Кузов же необходим, чтоб совершать перевозку погруженного внутрь 
          груза. Оперативность в работе достигается за счёт высокой маневренности, которой 
          обладает кран. Панель управления находится в кабине автомобиля. Если вам нужна аренда 
          автокрана обращайтесь в нашу компанию и получайте лучшую специализированную технику 
          в аренду по самой низкой в Киеве цене!</p>
          <h3>Особенности стоимостири услуги</h3>
        <p>Для заказа крана манипулятора, вам достаточно связаться с нами по контактам, указанным 
          на сайте. Мы предоставляем услуги по перевозке грузов по Киеву и за его пределами по 
          фиксированным тарифам. Однако перед размещением заказа на кран манипулятор, нужно 
          учитывать, что на себестоимость окажут влияние: место, где осуществляется погрузка 
          грузов; место их выгрузки; габариты и грузоподъёмность.</p>
        <p>Наша компания предлагает вам самые выгодные условия сотрудничества! Свяжитесь с нами 
          и мы сделаем вам выгодное предложение. Сотрудничая с нами, вы получаете специализированную 
          технику высокого качества по низкой цене. Заказывать у нас надёжно и выгодно. Мы ждём вашего 
          звонка!</p>
      </div>
    </div>

    <div class="row reviews">
      <div class="image-wrapper">
        <img src="<?php echo get_template_directory_uri(); ?>/img/Ellipse 1.png" alt="">
        <p>А. Ралдугина <br> Експресс-контракт</p> 
      </div>
      <div class="reviews-text-wrapper">
          <div class="title-wrapper">
            <h2>Отзывы наших клиентов</h2>
          </div>
          <div class="paragraph-wrapper">
            <p>Для того, чтобы перевозить практически все виды грузов, встречающихся при выполнении 
              строительных работ, вполне достаточно крана с грузоподъёмностью 5 тонн. Для максимального 
              использования этого потенциала достаточно арендовать такую машину как предоставили нам 
              коллеги з А-кран.</p>
          </div>
        </div>
    </div>
  </div>
</section>

<section class="gallery">
<div class="row"> 
   
    <div class="col-lg-3 col-md-4 col-6">
    <a data-fancybox="gallery" href="<?php echo get_template_directory_uri(); ?>/img/Rectangle 8.jpg">
        <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/Rectangle 8.jpg" alt="...">
    </a>
    </div>
    <div class="col-lg-3 col-md-4 col-6">
    <a data-fancybox="gallery" href="<?php echo get_template_directory_uri(); ?>/img/Rectangle 9.jpg">
        <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/Rectangle 9.jpg" alt="...">
    </a>
    </div>
    <div class="col-lg-3 col-md-4 col-6">
    <a data-fancybox="gallery" href="<?php echo get_template_directory_uri(); ?>/img/Rectangle 10.jpg">
        <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/Rectangle 10.jpg" alt="...">
    </a>
    </div>
    <div class="col-lg-3 col-md-4 col-6">
    <a data-fancybox="gallery" href="<?php echo get_template_directory_uri(); ?>/img/Rectangle 11.jpg">
        <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/Rectangle 11.jpg" alt="...">
    </a>
    </div>
    <div class="col-lg-3 col-md-4 col-6">
    <a data-fancybox="gallery" href="<?php echo get_template_directory_uri(); ?>/img/Rectangle 12.jpg">
        <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/Rectangle 12.jpg" alt="...">
    </a>
    </div>
    <div class="col-lg-3 col-md-4 col-6">
    <a data-fancybox="gallery" href="<?php echo get_template_directory_uri(); ?>/img/Rectangle 13.jpg">
        <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/Rectangle 13.jpg" alt="...">
    </a>
    </div>
    <div class="col-lg-3 col-md-4 col-6">
    <a data-fancybox="gallery" href="<?php echo get_template_directory_uri(); ?>/img/Rectangle 14.jpg">
        <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/Rectangle 14.jpg" alt="...">
    </a>
    </div>
    <div class="col-lg-3 col-md-4 col-6">
    <a data-fancybox="gallery" href="<?php echo get_template_directory_uri(); ?>/img/Rectangle 15.jpg">
        <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/Rectangle 15.jpg" alt="...">
    </a>
    </div>
    <!-- <div class="col-lg-6 col-md-6 col-6">
    <a data-fancybox="gallery" href="<?php echo get_template_directory_uri(); ?>/img/Rectangle 16.jpg">
        <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/Rectangle 16.jpg" alt="...">
    </a>
    </div>
    <div class="col-lg-6 col-md-6 col-6">
    <a data-fancybox="gallery" href="<?php echo get_template_directory_uri(); ?>/img/Rectangle 17.jpg">
        <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/Rectangle 17.jpg" alt="...">
    </a>
    </div> -->
</div>
</section>

<?php get_footer(); ?>
   