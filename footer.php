    <footer>
    <div class="container">
        <div class="row footer-navbar">
        <nav class="navbar navbar-expand-lg">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent2" aria-controls="navbarSupportedContent2" aria-expanded="false" aria-label="Toggle navigation">
              <span class="fa fa-bars"></span>
            </button>

        <?php wp_nav_menu(array(
            'menu'       => 'bottom-menu',
            'menu_class' => 'navbar-nav', 
            'depth' => 2,
            'container_id' => 'navbarSupportedContent2',
			'container_class' => 'collapse navbar-collapse',
            'items_wrap' => '<ul id="%1$s" class="%2$s" >%3$s</ul>',
            'walker'	 => new Bootstrap_Walker_Nav_Menu()
        ));?> 
  
            </nav>
        </div>
        <div class="row footer-contacts-wrapper">
            <div class="footer-img">
                <img src="<?php echo get_template_directory_uri(); ?>/img/A-Kran.png" alt="">
            </div>
            <div class="footer-contacts">
                <h3>Наши контакты:</h3>
                <p>По вопросам крана-манипулятора звоните:</p>
                <h4>(068) 870-25-45</h4>
            </div>
            <div class="footer-tel">
            <p>По вопросам аренды бытовок звоните: hh </p>
            </div>
        </div>
    </div>
    </footer>
    <?php wp_footer(); ?>
</body>
</html>